# pixiv_down

A CLI tool for downloading content from [pixiv].

[pixiv]: https://www.pixiv.net

## Requirements

D >= `2.076.0`
: https://dlang.org

GraphicsMagick >= `1.3.0`
: http://www.graphicsmagick.org (only for non-macOS POSIX)

You will also need either [Meson], [SCons], or [dub] (version 1.23.0 or newer).

pixiv_down is only tested on Linux and macOS.

## Build

> [!NOTE]
> Building on macOS with LDC requires LDC 1.40 and newer.

It is possible to build pixiv_down using dub by running `dub build`.

If you do not have `dub`, or simply don't want to use it, the SCons build
system can be used:

```bash
scons
```

The compiled executable can be found at `source/pixiv_down`.

You can install the executable by running the following as root:

```bash
scons install
```

For more information, see the `INSTALL` file.

## Usage

An expanded version of the help message. Some options may be omitted,
please run `pixiv_down help <command>` for more information on any of
the commands.

```txt
Usage: pixiv_down <command> <arguments> [options]

Commands:
  artist [-t|--type TYPE] <id> [<id2>, <id3>, ...]

      Download all content of TYPE from the provided account ID(s).
      TYPE can be either manga, illust, or novel, depending on what
      you want to download.

  artwork [--group-errors] <id> [<id2>, <id3>, ...]

      Download an artwork (or manga) with ID <id>.  You can download
      multiple items by passing multiple IDs separated by a space.

      The --group-errors option will change the behaviour such that
      instead of printing any errors after each ID, all errors will
      be printed after attempting to download the last ID.

  bookmarked [--novels] [--private] [--remove-invalid]
             [-s|--skip OFFSET]

      Download all of your bookmarked works.  By default, this will download
      all of the illustrations, manga, and ugoira on your 'public' bookmark
      list.  You can pass the --novels flag to download the novels instead.
      Using the --private flag will cause pixiv_down to download from your
      'private' bookmark list.

      After running this command, a list of IDs will be written to a file
      called 'pixiv_down-missing.txt'.  These IDs represent the works that
      are no longer available on pixiv.  You can pass the --remove-invalid
      flag and pixiv_down will remove these works from your bookmark list.

  compact [-i|--interactive] [-n|--dry-run]

      Go through all the accounts you have downloaded and remove
      duplicated content.  When downloading, pixiv_down creates
      a directory using the current account's display name, which
      can change.  The compact command will move any content from
      the old directories (previous display names) in to the
      directory using the current display name, so long as the
      content is not already present.

      The --interactive option will allow you to choose which
      directory name to use.  The --dry-run option will print
      out the actions that would take place, without actually
      performing the actions.

  daily [-e|--end END] [-b|--begin BEGIN] [-s|--sfw-only]

      Download the latest content from all the accounts you follow.
      The END option specifies the date to stop downloading at. The
      default behaviour is to download everything from the time of
      execution until END. You can optionally specify BEGIN to change
      the date pixiv_down begins downloading from. Both BEGIN and
      END expect a date in the format YYYY-MM-DD.

      An END date will be determined automatically if you have
      previously called 'daily'.  It will use the BEGIN of the
      previous run, regardless of if a manual date was provided
      or not.

      The --sfw-only option restricts the content that is downloaded
      to that which is not marked as R-18.

  following <--public|--private> [-s|--skip NUMBER]

      Download all content from the accounts you follow, in order of
      most recently followed. You must specify whether to download
      from accounts you publically follow (--public) or the accounts
      you follow in private (--private).

      If you know that you have already downloaded the first 3
      accounts, you can use the --skip option (--skip 3) which
      will cause pixiv_down to skip over the first 3 accounts.

  novel [--group-errors] <id> [<id2> <id3> ...]

      Download the novel with ID <id>.  You can download multiple
      items by passing multiple IDs separated by a space.

      The --group-errors option will change the behaviour such that
      instead of printing any errors after each ID, all errors will
      be printed after attempting to download the last ID.

  prune [-n|--dry-run] [-q|--quiet]

      Go through all the accounts you have downloaded and remove
      directories for accounts that you no longer follow, or no
      longer exist (i.e. the account has been deleted).

      You will be prompted to remove an account's directories unless
      the --quiet option is used, in which case the directories will
      be removed automatically.

      Passing the --dry-run option will still prompt to remove
      the directories, however, it will not actually remove them.
```

## Notice

pixiv_down does _not_ allow you to download items which you should
not be allowed to.  By this I mean, if there is something on pixiv
that you must pay for in order to access, then pixiv_down won't
allow you to download it unless you have paid for it.  I'm not aware
of anything on pixiv that requires payment, but just a heads up.

It should go without saying, but this is an unofficial program and any usage
_may_ result in your account being suspended or terminated.

## License

pixiv_down is licensed under the terms of the GNU General Public License,
version 3.  You can find a copy in the COPYING file, or online at
<https://www.gnu.org/licenses/gpl-3.0.html>.

[dub]: https://dub.pm
[SCons]: https://scons.org
[Meson]: https://mesonbuild.com
[image-io]: https://developer.apple.com/documentation/imageio
