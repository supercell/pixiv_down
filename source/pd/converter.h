/*
   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <https://unlicense.org>
*/

/**
   This file defines the data types and function signatures that pixiv_down
   will call when loading a plugin.

   The functions you are required to export are listed at the bottom of this
   file, with documentation so you can get started.
**/
#ifndef PD_CONVERTER_H
#define PD_CONVERTER_H

#include <stdbool.h>

#define PD_CONVERTER_API_VERSION 1

typedef struct ConverterInfo ConverterInfo;
typedef struct Converter Converter;

struct ConverterInfo
{
    /* Name of the converter. */
    const char *name;

    /* Converter API version that this plugin supports.
     *  Should be set to PD_CONVERTER_API_VERSION. */
    unsigned api_version;
    unsigned reserved; /* padding */

    /* Create a new Converter that will output \param format. *
     * Should return NULL if it failed to create.             */
    Converter *(*create_converter)(const char* format);
};

struct Converter
{
    bool (*append_frame)(Converter*, const char* path);
    void (*set_frame_delay)(Converter*, unsigned delay_ms); /*< Applies to the most recently appended frame */
    bool (*write)(Converter*, const char* path);
    void (*dispose)(Converter*);
};


#if 0

/**
   The below functions should be exported in your shared library (with C linkage).
**/

/**
   Perform any initialisation that the library requires before using the
   converter.

   Deinitialisation happens after the command has run, regardless of if it was
   successful or not.
**/
bool initialize_converter_library(ConverterInfo* info);
void deinitialize_converter_library(ConverterInfo* info);

/**
   Create a new converter that pixiv_down can use.

   The returned Converter must have all the functions set.
   
   @param format The output format.
   @return NULL if failed, otherwise a Converter.
**/
//Converter* create_converter(const char *format);

#endif /* if 0 */

#endif /* PD_CONVERTER_H */
