;; Copyright (C) 2024 mio <stigma@disroot.org>
;;
;; This file is part of pixiv_down.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;
;; This is a GNU Guix package description that can be used for spawning
;; new development shells.  Simply run:
;;
;;  guix shell -D -f guix.scm
;;
;; To run pixiv_down in a container you'll need network access and to
;; expose the output directory specified in your settings.conf:
;;
;;  guix shell -C --network --share=/home/user/Pictures -D -f guix.scm
;;
;; In the new shell, run:
;;
;;  ./bootstrap && ./configure && make
;;

(use-modules (guix packages)
	     (guix gexp)
	     (guix licenses)
	     (guix git-download)
	     (guix build-system meson)
	     (gnu packages certs)
	     (gnu packages curl)
	     (gnu packages gcc)          ;; gdc
	     (gnu packages imagemagick)
	     (gnu packages pkg-config))

(define %source-dir (dirname (current-filename)))

(package
 (name "pixiv_down")
 (version "git")
 (source (local-file %source-dir
		     #:recursive? #t
		     #:select? (git-predicate %source-dir)))
 (build-system meson-build-system)
 (arguments
  (list #:tests? #f))
 (native-inputs
  (list
   gdc
   pkg-config))
 (inputs
  (list
   curl
   graphicsmagick
   nss-certs))
 (home-page "https://yume-neru.neocities.org/p/pixiv_down.html")
 (synopsis "A command-line tool for downloading content from pixiv")
 (description
  "A command-line tool for downloading different types of works from pixiv.
Supports downloading illustrations, ugoira, manga, and novels.")
 (license gpl3))
